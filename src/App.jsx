import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Login, Signup,Home, Recipe, Favourites, Courses, Community} from './pages/'
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import {UserProvider} from './contexts/UserContext'


import './App.css';
import { MainContainer, SideBar } from './layouts';

function App() {

  return (
    <div className="App">
        <UserProvider>
            <Router>
            <SideBar/>
            <MainContainer>
                <Routes>
                    <Route path='/' element={<Login/>}/>
                    <Route path='*' element={<Signup/>} />
                    <Route path="home" element={<Home />} />
                    <Route path="recipes" element={<Recipe />} />
                    <Route path='favourites' element={<Favourites/>} />
                    <Route path='courses' element={<Courses/>} />
                    <Route path='community' element={<Community/>} />
                </Routes>
            </MainContainer>
            </Router>
        </UserProvider>
    </div>
  )
}

export default App
