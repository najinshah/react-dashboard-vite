import React from 'react'

function Rating({value, icon:Icon}) {
    return (
        <>
            {[...Array(5)].map((el, i) => {
                return <Icon className={value-1 >= i ? 'active' :'inactive'} key={i}/> 
            })}
        </>
    )
}

export default Rating