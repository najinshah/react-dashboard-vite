import React from 'react';
import { SiCrystal } from "react-icons/si"

import { fullName } from '../../utils';

function SbProfile({style: styles, userDetails, className}) {
  return (
    <div className={className}>
        <div className={`${styles.image} circular large`} style={{backgroundImage: `url(${userDetails?.picture?.large})`}}>
        </div>
        {/* className=[styles.description, styles.yellow].join(' ') */}
        <h4 className={`${styles.name} text_custom d-none d-md-block`}>{fullName(userDetails.name?.first, userDetails.name?.last)}</h4>
        <h5 className={`job text muted d-none d-md-block ${styles.job}`}> Chef de Partie</h5>
        <div className={`d-none d-md-flex `+ styles.badge}>
            <SiCrystal/>
            <span>
                {Math.floor(Math.random() * 78)}
            </span>
        </div>
    </div>
  )
}

export default React.memo(SbProfile)