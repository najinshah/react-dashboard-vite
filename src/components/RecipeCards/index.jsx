import React from 'react';
import { FaStar, FaFish, FaLeaf} from 'react-icons/fa';
import {Rating} from '../../components'
import {GiCherry} from 'react-icons/gi'

function RecipeCards({recipe}) {
    const getTypeIcons = (typ) => {
        return typ === 'Fish' ? <FaFish/> :
            typ === 'Meat' ? <FaFish/> :
            typ === 'Fruit' ? <GiCherry/> :
            typ === 'Veg' ? <FaLeaf/> :
            <FaFish/>

    }
  return (
    <>
        <div className="card_wrapper me-4">
            <div className="recipe_card">
                <img src={recipe.imageURL} alt="image" onError={(e) => e.target.src = 'https://i.stack.imgur.com/WHCGy.png'}/>
                <h6 className='fw-bold text-center text-xl-start'>{recipe.name}</h6>
                <div className="my-1 p-1 recipe_level" data-level={recipe.level.charAt(0)}>{recipe.level}</div>
                <div className="recipe_numbers d-flex justify-content-between my-4">
                    <div>
                        <h5 className='m-0 fs-4 fw-bold'>{recipe.time}</h5>
                        <div className="text muted">Min</div>
                    </div>
                    <div className='border-start border-end'>
                        <h5 className='m-0 fs-4 fw-bold'>{recipe.calories}</h5>
                        <div className="text muted">Kcal</div>
                    </div>
                    <div>
                        <h5 className='m-0 fs-4 fw-bold' data-cont={recipe.type}>{getTypeIcons(recipe.type)}</h5>
                        <div className="text muted">{recipe.type}</div>
                    </div>
                </div>
                <div className="recipe_rating mb-4" data-rating={recipe.rating}>
                    <Rating value ={recipe.rating} icon={FaStar}/>
                </div>
                <div className="recipe_card_btn border-top">
                    <button className='w-100 px-1 py-3 fw-bold text muted'>
                        Start cooking
                    </button>
                </div>
            </div>
        </div>
    </>
  )
}

export default React.memo(RecipeCards)