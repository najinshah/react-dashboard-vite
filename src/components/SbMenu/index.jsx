import React, { useState } from 'react';
import { NavLink, useLocation  } from "react-router-dom";

import { FaBookOpen, FaHeart,FaSuitcase, FaGlobe, FaHotel, FaHome  } from 'react-icons/fa'
import sbMenuStyles from './sbmenu.module.css'

function SbMenu({items, ...props}){
    
    const [sidebarMenu, setMenuItems] = useState(()=> items)
    const location = useLocation()

    function getIcons(name){
        return (
            name === 'Home' ? <FaHome /> :
            name === 'Recipes' ? <FaBookOpen /> :
            name === 'Favourites' ? <FaHeart /> :
            name === 'Courses' ?<FaSuitcase />  :
            name === 'Community' ? <FaGlobe /> :
            <FaHotel/>
        )
    }
    const getActiveState = (name) => location.pathname === '/'+name.toLowerCase()? 'active':'';
    
    return(
        <>
            {sidebarMenu.map(menu => {
                let {id, name} = menu
                return (
                    <NavLink key={id} to={`/${name.toLowerCase()}`} style={{textDecoration: 'none'}} className={`${getActiveState(name)} ${sbMenuStyles.link}`}>
                        <div name={name} className={`sbmenu justify-content-center justify-content-md-start ${sbMenuStyles.wrapper}`} >
                            <div className={`${sbMenuStyles.icon} icon`}>  
                                {getIcons(name)}
                            </div>
                            <div className="name d-none d-md-block">
                                {name}
                            </div>
                        </div>
                    </NavLink>
                )
            })}
        </>
    )
}

export default React.memo(SbMenu)