import { createContext, useState, useEffect, useContext } from "react";

const UserContext = createContext()

export function UserProvider({children}){
    const [user, setUser] = useState({})
    const [userLoaded, setUserLoaded] = useState(false)
    useEffect(() => {
        if(!Object.keys(user).length){
            fetch("https://randomuser.me/api/")
            .then(res => res.json())
            .then(result => {
                setUser(result.results[0])
               
            }).then(
                setUserLoaded(true)
            )
        }

    }, [user])

    return(
        <UserContext.Provider value={user}>
            {children}
        </UserContext.Provider>
    )
    
}

export const useUserContext= () => useContext(UserContext)