import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom";
import { GiSettingsKnobs} from 'react-icons/gi'
import { FaSearch, FaCloudMeatball, FaStar} from 'react-icons/fa';
import noodles from '../../assets/images/noodles.svg';
import { recipes } from '../../data/data';
import './Home.css'
import { RecipeCards } from '../../components';


function Home() {
    const[inputVal, setInputVal] = useState('')
    const[filter, setFilter] = useState('')

    const handleChange = (val) => {
        setInputVal(val)
        if(val.length <1){
            setFilter(recipes)
        }
    }
    const handleSearch = (e) => {
        if(e.key === 'Enter'){
            let newdata = recipes.filter(x => x.name.toLowerCase().includes(inputVal))
            setFilter(newdata)
        }
    }
    useEffect(()=> {
        setFilter(recipes)
    },[])
  return (
    <>
        <div className="mc_wrapper">
            {/* ==================top search=================== */}
            <div className="page_top d-flex justify-content-between mb-5 py-2">
                <div className="top_serach">
                    <FaSearch/>
                    <input
                        value={inputVal}
                        placeholder='Enter your search request'
                        onChange={(e) => handleChange(e.target.value)}
                        onKeyPress ={(e) => handleSearch(e)}
                    />
                </div>
                <div className="top_setting">
                        <GiSettingsKnobs />
                    <button className="large with_bg">
                        Go to Premium
                    </button>
                </div>
            </div>
            {/* ==================top search=================== */}
            {/* ==================top title=================== */}
            <div className="home_phrase_wrapper d-flex justify-space-between align-items-center py-2 flex-wrap my-5 my-md-4">
                <div className="d-flex me-auto">
                    <div className="icon_wrapper">
                        {/* <div className="icon w-50"> */}
                            <img className='w-100' src={noodles} alt="cooking"/>
                        {/* </div> */}
                    </div>
                    <div className="words text_large ps-3">
                        <h1 className="text text_custom">Only the best reipes!</h1>
                        <div className="text muted">Today's new recipe for you</div>
                    </div>
                </div>
                <div className="counts d-none d-lg-flex">
                    <div className='px-3 text-center border-end'>
                        <h2 className='text_custom'>168</h2>
                        <div className='text muted'>Tutorials</div>
                    </div>
                    <div className='px-3 text-center'>
                        <h2 className='text_custom'>304</h2>
                        <div className='text muted'>Recipes</div>
                    </div>

                </div>
            </div>

            {/* ==================top title=================== */}
            {/* ==================top content=================== */}

            <div className="mc_home_content d-flex flex-wrap justify-content-center justify-content-lg-start">
                {filter && filter.map((x, i) => {
                    return <RecipeCards key={i} recipe={x} />
                })}
            </div>
        </div>
        
    </>
  )
}

export default Home