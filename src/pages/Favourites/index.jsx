import React from 'react'
import { Link } from 'react-router-dom'
import { SideBar, MainContainer } from '../../layouts'

function Favourites() {
  return (
    <>
        <MainContainer>
            <Link to="home">home</Link>
            <div>Favourites</div>
        </MainContainer>
    </>
  )
}

export default Favourites