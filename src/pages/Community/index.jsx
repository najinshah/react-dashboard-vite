import React from 'react'
import { Link } from 'react-router-dom'
import { MainContainer, SideBar } from '../../layouts'

function Community() {
  return (
      <>
        <MainContainer>
            <Link to="home">home</Link>
            <div>Community</div>
        </MainContainer>
      </>
  )
}

export default Community