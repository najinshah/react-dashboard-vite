export function clx(...classes) {
    return classes.join(" ");
  }

export function fullName(fname, lname, title) {
    return `${title??''} ${fname??''} ${lname??''}`;
}