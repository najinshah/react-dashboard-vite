import React from 'react';
import './maincontainer.css'

function MainContainer({children}) {
  return (
    <div id="Maincontainer">
        {children}
    </div>
  )
}

export default MainContainer