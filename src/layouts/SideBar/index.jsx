import React, {useState} from 'react';


import {menus} from '../../data/data';
import {SbMenu,SbProfile} from '../../components';
import {useUserContext} from '../../contexts/UserContext';
import logo from '../../assets/images/foodoo.png'
import styles from './Sidebar.module.css';


function SideBar() {

const [sidebarMenu, setSidebarMenu] = useState(menus)
const userDetails = useUserContext()

   
  return (
    <div id="Sidebar" className={styles.sidebar}>
        <div className={`${styles.sb_wrapper} h-100 justify-content-md-between`}>
            <div className={styles.logo} style={{backgroundImage: `url(${logo})`}} />
                <SbProfile className={styles.profile_wrapper + ' ' + 't-center w-100'} style={styles} userDetails={userDetails} />                
            <div className={`${styles.menu_wrapper} navigation`}>
                <SbMenu items ={sidebarMenu}/>
            </div>
            <div className={styles.nav_footer+ " " + 't-center mt-auto pb-3 pb-md-0'}>
                <div className='t-center'>create a team and take part</div>
            </div>
        </div>

    </div>
  )
}

export default (SideBar)